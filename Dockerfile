FROM eclipse-temurin:17.0.6_10-jdk-focal as builder
COPY . /src
WORKDIR /src
RUN ls /src
RUN ls /src/target

FROM alpine:3.16.0 as packager
RUN apk --no-cache add binutils openjdk17-jdk openjdk17-jmods
ENV JAVA_MINIMAL="/opt/java-minimal"
RUN /usr/lib/jvm/java-17-openjdk/bin/jlink \
    --verbose \
    --add-modules \
        jdk.jfr,jdk.management.agent,java.base,java.logging,java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument,jdk.zipfs \
    --compress 2 --strip-debug --no-header-files --no-man-pages \
    --release-info="add:IMPLEMENTOR=radistao:IMPLEMENTOR_VERSION=radistao_JRE" \
    --output "$JAVA_MINIMAL"

FROM alpine:3.16.0
LABEL maintainer="Vyacheslav Botalov Vyacheslav.Botalov@gmail.com"
ENV JAVA_HOME=/opt/java-minimal
ENV PATH="$PATH:$JAVA_HOME/bin"
COPY --from=packager "$JAVA_HOME" "$JAVA_HOME"
COPY --from=builder /src/target/service-discovery.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Xmx384m","-Dserver.port=8080","-jar","/app.jar"]