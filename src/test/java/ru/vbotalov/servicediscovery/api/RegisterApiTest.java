package ru.vbotalov.servicediscovery.api;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.vbotalov.servicediscovery.api.requests.RegisterRequest;
import ru.vbotalov.servicediscovery.model.Topic;
import ru.vbotalov.servicediscovery.services.DiscoveryService;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;
import static ru.vbotalov.servicediscovery.TestData.*;

public class RegisterApiTest {

    private RegisterApi api;

    private DiscoveryService discoveryService;

    @BeforeEach
    public void before() {
        discoveryService = Mockito.mock(DiscoveryService.class);

        api = new RegisterApi(discoveryService);
    }

    @Test
    public void shouldRegisterSuccess() {
        when(discoveryService.register(
                TOPIC_ID,
                TOPIC_NAME,
                TOPIC_SERVICE_URL
        )).thenReturn(getTestTopic());

        var request = new RegisterRequest();
        request.setTopicId(TOPIC_ID);
        request.setTopicName(TOPIC_NAME);
        request.setServiceUrl(TOPIC_SERVICE_URL);

        var response = api.register(request);

        assertTrue(response.isSuccess());
        assertEquals(TOPIC_ID, response.getTopic().getId());
        assertEquals(TOPIC_NAME, response.getTopic().getName());
        assertEquals(TOPIC_SERVICE_URL, response.getTopic().getServiceUrl());
        assertTrue(response.getTopic().isAvailable());
    }

    @Test
    public void shouldGetTopicsSuccess() {
        when(discoveryService.getTopics()).thenReturn(
                Collections.singletonList(
                        getTestTopic()
                )
        );

        var response = api.getTopics();

        assertTrue(response.isSuccess());
        assertEquals(1, response.getTopics().size());
        assertEquals(TOPIC_ID, response.getTopics().get(0).getId());
        assertEquals(TOPIC_NAME, response.getTopics().get(0).getName());
        assertEquals(TOPIC_SERVICE_URL, response.getTopics().get(0).getServiceUrl());
        assertTrue(response.getTopics().get(0).isAvailable());
    }

    private Topic getTestTopic() {
        var topic = new Topic(TOPIC_ID, TOPIC_NAME);
        topic.setServiceUrl(TOPIC_SERVICE_URL);
        topic.setAvailable(true);
        return topic;
    }

}
