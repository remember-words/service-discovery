package ru.vbotalov.servicediscovery;

public final class TestData {

    public static final String TOPIC_ID = "topic-id";
    public static final String TOPIC_NAME = "topic-name";
    public static final String TOPIC_SERVICE_URL = "https://service:port";

}
