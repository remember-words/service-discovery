package ru.vbotalov.servicediscovery.api.responses;

import ru.vbotalov.servicediscovery.model.Topic;

import java.util.List;

public class GetTopicsResponse extends DefaultResponse {

    private final List<Topic> topics;

    public GetTopicsResponse(List<Topic> topics) {
        this.topics = topics;
    }

    public List<Topic> getTopics() {
        return topics;
    }
}
