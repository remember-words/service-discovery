package ru.vbotalov.servicediscovery.api.responses;

import ru.vbotalov.servicediscovery.model.Topic;

public class RegisterResponse extends DefaultResponse {

    private final Topic topic;

    public RegisterResponse(Topic topic) {
        this.topic = topic;
    }

    public Topic getTopic() {
        return topic;
    }
}
