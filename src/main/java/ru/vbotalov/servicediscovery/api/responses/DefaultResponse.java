package ru.vbotalov.servicediscovery.api.responses;

public class DefaultResponse {

    private final boolean success;
    private final String message;

    public DefaultResponse() {
        success = true;
        message = null;
    }

    public DefaultResponse(String message) {
        success = false;
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
