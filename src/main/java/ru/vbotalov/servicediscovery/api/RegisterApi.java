package ru.vbotalov.servicediscovery.api;

import org.springframework.web.bind.annotation.*;
import ru.vbotalov.servicediscovery.api.requests.RegisterRequest;
import ru.vbotalov.servicediscovery.api.responses.GetTopicsResponse;
import ru.vbotalov.servicediscovery.api.responses.RegisterResponse;
import ru.vbotalov.servicediscovery.services.DiscoveryService;

@RestController
@RequestMapping(path = "/api/v1/discovery")
public class RegisterApi {

    private final DiscoveryService discoveryService;

    public RegisterApi(DiscoveryService discoveryService) {
        this.discoveryService = discoveryService;
    }

    @PostMapping(path = "/register")
    public RegisterResponse register(@RequestBody RegisterRequest request) {
        var topic = discoveryService.register(request.getTopicId(),
                request.getTopicName(),
                request.getServiceUrl());
        return new RegisterResponse(topic);
    }

    @GetMapping(path = "/topics")
    public GetTopicsResponse getTopics() {
        return new GetTopicsResponse(discoveryService.getTopics());
    }
}
