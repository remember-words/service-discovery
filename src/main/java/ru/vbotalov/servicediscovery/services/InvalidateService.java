package ru.vbotalov.servicediscovery.services;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.vbotalov.servicediscovery.config.AppProperties;

import java.time.LocalDateTime;

@Component
public class InvalidateService {

    private final DiscoveryService discoveryService;
    private final int ttl;

    public InvalidateService(DiscoveryService discoveryService, AppProperties properties) {
        this.discoveryService = discoveryService;
        this.ttl = properties.getTtl();
    }

    @Scheduled(fixedRate = 60)
    public void invalidate() {
        var now = LocalDateTime.now();
        discoveryService.invalidate(now, ttl);
    }
}
