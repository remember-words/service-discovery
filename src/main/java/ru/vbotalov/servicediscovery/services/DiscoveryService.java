package ru.vbotalov.servicediscovery.services;

import ru.vbotalov.servicediscovery.model.Topic;

import java.time.LocalDateTime;
import java.util.List;

public interface DiscoveryService {

    Topic register(String id, String name, String serviceUrl);

    List<Topic> getTopics();

    void invalidate(LocalDateTime now, int ttl);

}
