package ru.vbotalov.servicediscovery.services.local;

import org.springframework.stereotype.Service;
import ru.vbotalov.servicediscovery.model.Topic;
import ru.vbotalov.servicediscovery.services.DiscoveryService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class InMemoryDiscoveryService implements DiscoveryService {

    private static final Map<String, Topic> TOPICS = new ConcurrentHashMap<>();

    @Override
    public Topic register(String id, String name, String serviceUrl) {
        var topic = TOPICS.computeIfAbsent(id, $ -> new Topic(id, name));
        topic.setAvailable(true);
        topic.setServiceUrl(serviceUrl);
        topic.setLiveTime(LocalDateTime.now());
        return topic;
    }

    @Override
    public List<Topic> getTopics() {
        return new ArrayList<>(TOPICS.values());
    }

    @Override
    public void invalidate(LocalDateTime now, int ttl) {
        TOPICS.values().forEach(topic -> {
            if (isExpired(topic, now, ttl)) {
                topic.setAvailable(false);
            }
        });
    }

    private boolean isExpired(Topic topic, LocalDateTime now, int ttl) {
        var seconds = ChronoUnit.SECONDS.between(topic.getLiveTime(), now);
        return seconds >= ttl;
    }
}
