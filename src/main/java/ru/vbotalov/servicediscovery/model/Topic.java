package ru.vbotalov.servicediscovery.model;

import java.time.LocalDateTime;

public class Topic {

    private final String id;
    private final String name;
    private String serviceUrl;
    private boolean available;
    private LocalDateTime liveTime;

    public Topic(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public LocalDateTime getLiveTime() {
        return liveTime;
    }

    public void setLiveTime(LocalDateTime liveTime) {
        this.liveTime = liveTime;
    }
}
